<?php

define('URL', 'http://localhost/food/');
define('JSON', dirname($_SERVER['SCRIPT_FILENAME']) . '/db.json');

// dd();

function br(){
    echo '<br>';
}
function tag($tag = 'pre'){
    echo '<'. $tag .'>';
}
function redirect_to($path = ''){
    $path = URL . $path;
    header('Location: '. $path);
}
function dd($variable){
    tag();
    print_r($variable);
    exit();
}
function beautify_json($json){
    $content = json_encode($json, JSON_PRETTY_PRINT|JSON_UNESCAPED_UNICODE);
    $content = trim($content, "\"");
    return str_replace(['\r\n', '\n', "\\\""], ["\n", "\n", "\""] ,$content);
}
function save_json($array, $json_file = JSON){
    $beautified_json = beautify_json(json_encode($array, JSON_PRETTY_PRINT|JSON_UNESCAPED_UNICODE));
    file_put_contents($json_file, $beautified_json);
}