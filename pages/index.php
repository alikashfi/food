<!DOCTYPE html>
<html lang="fa">
<head>
    <meta charset="UTF-8">
    <title>لیست غذاها</title>
    <script src='<?php echo URL;?>pages/jquery.js'></script>
    <link rel="manifest" href="/manifest.json" />
    <meta name="viewport" content="width=device-width">
</head>
<body dir="rtl">

<?php foreach($tables as $category => $table): ?>
<div class="row">
<p class="space"> </p>
    <table class="table__">
        <thead>
            <tr>
                <th class="w10__">#</th>
                <th>
                <?php
                echo ($category == 'breakfast') ? 'صبحانه' : null;
                echo ($category == 'lunch') ? 'نهار' : null;
                echo ($category == 'dinner') ? 'شام' : null;
                echo ($category == 'confection') ? 'خوراکی' : null;
                ?>
                <a class='but add' href="add?category=<?php echo $category; ?>">+</a></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach($table as $key => $food): ?>
            <tr>
                <td><?php echo $key + 1; ?></td>
                <td><?php echo $food->name; ?><a class='but delete' href="delete?id=<?php echo $key ?>&category=<?php echo $category; ?>">+</a><a class='but update' href="edit?id=<?php echo $key ?>&category=<?php echo $category; ?>">✎</a></td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
</div>
<?php endforeach; ?>


<style>
@font-face{
  font-family: "IRANSANSweb";
  src: url("<?php echo URL;?>/IRANSansWeb.ttf") format("truetype"),
       url("<?php echo URL;?>/IRANSansWeb.woff2") format("woff2");
}
body{
    background: #fff;
    font-family: "IRANSANSweb";
}
.row{
  display: inline-block;
  width: 30%;
  margin: 0 1.5%;
}
.table__ {
  text-align: left;
  margin: auto;
  width: 100%;
  border-collapse: collapse;
  overflow: hidden;
  border-radius: 10px;
}
.table__ thead {
  border-radius: 10px 10px 0 0;
  overflow: hidden;
  color: #fff;
  text-align: right;
}
.table__ thead th {
  padding: 15px 20px;
  background-color: #6c7ae0;
}
.table__ tbody td {
  padding: 12px 20px;
  color: grey;
  text-align: right;
}
.table__ tbody tr {
  border-right: 1px solid #eee;
  border-left: 1px solid #eee;
}
.table__ tbody tr:nth-child(2n) {
  background-color: #f8f6ff;
}
.table__ tbody tr:last-child {
  border-bottom: 1px solid #eee;
  border-radius: 10px;
}
.table__ tbody tr {

  transition: background 0.2s
}
.table__ tbody tr:hover {
  background: #eee
}
.w10__ {
  width: 10px
}
.but{
    display: inline-block;
    margin: 0;
    float: left;
    display: none;
    text-decoration: none;
    transition: ease color .2s;
}
.add{
    transform: scale(1.5);
    color: white;
}.add:hover{color: lawngreen}
.delete{
    color: grey;
    transform: rotate(45deg) scale(1.7);
}.delete:hover{color: red}
.update{
    color: grey;
    margin-left: 4%;
    transform: scale(1.3) translateY(.5px);
}.update:hover{color: #303030}
@media only screen and (max-width: 721px) {
  .row{
    width: 97%;
  }
  .space {
    height: 0;
  }
}
</style>

<script>
$(document).ready(function(){
    $('thead').hover(function(){
        $('.add').fadeIn(100);
    }, function(){
        $('.add').fadeOut(100);
    }
    );
    
    $('td').hover(function(){
        $(this).find('.delete').fadeIn(100);
    }, function(){
        $(this).find('.delete').fadeOut(100);
    }
    );
    
    $('td').hover(function(){
        $(this).find('.update').fadeIn(100);
    }, function(){
        $(this).find('.update').fadeOut(100);
    }
    );
});
</script>
<script>
  if ('serviceWorker' in navigator) {
    window.addEventListener('load', function() {
      navigator.serviceWorker.register('/service-worker.js');
    });
  }
</script>
</body>
</html>

