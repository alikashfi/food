<?php

if (isset($_GET["id"])) {
    $category = $_GET['category'];
    $id = $_GET["id"];
    $json_file = file_get_contents(JSON);
    $json_content = json_decode($json_file, true);
    $data = $json_content[$category];

    if ($data[$id]) {
        unset($json_content[$category][$id]);
        $json_content[$category] = array_values($json_content[$category]);
        save_json($json_content);
    }
    redirect_to('');
}