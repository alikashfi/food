<?php
// TODO : beautify for methods: delete, update       && remove this line
require_once 'helpers.php'; //and config

if(isset($_GET['url'])){
    switch($_GET['url']){
        case 'index':
            require 'index.php';
            break;
        case 'add':
            require 'add.php';
            break;
        case 'edit':
            require 'edit.php';
            break;
        case 'delete':
            require 'delete.php';
            break;
        case 'manifest.json':
            header('Content-Type: application/json; charset=utf-8');
            require 'pages/manifest.json';
            break;
        case 'service-worker.js':
            header('Content-Type: text/javascript; charset=utf-8');
            require 'pages/service-worker.js';
            break;
        default:
            echo 'this is default in router!';
            break;
    }
}