<?php
if (isset($_GET["id"])) {
    $id = $_GET["id"];
    $category = $_GET["category"];
    $json_file = file_get_contents(JSON);
    $json_content = json_decode($json_file, true);
    $category_data = $json_content[$category];
    $row = $category_data[$id];
}

if (isset($_POST["id"])) {
    $category = $_POST['category'];
    $id = $_POST["id"];

    $json_file = file_get_contents(JSON);
    $json_content = json_decode($json_file, true);
    $category_content = $json_content[$category];
    $row = $category_content[$id]; //the row must be updated

    $data["name"] = isset($_POST["name"]) ? $_POST["name"] : "";



    if ($row) {
        $json_content[$category][$id] = $data;
        $json_content[$category] = array_values($json_content[$category]);
        save_json($json_content);
    }
    redirect_to(); //home
}
?>
<?php if (isset($_GET["id"])): ?>
    <form action="" method="POST">
        <input type="text" value="<?php echo $row['name'] ?>" name="name" style="direction:rtl" autofocus>
        <input type="hidden" value="<?php echo $id ?>" name="id">
        <input type="hidden" value="<?php echo $_GET['category'] ?>" name="category">
        <input type="submit"/>
    </form>
<?php endif; ?>